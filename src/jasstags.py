import sys, io, re

def pattern_get():
    return re.compile('^(constant\s+)*(native|function)\s+(?P<tag>\w+)\s+takes')
    #return re.compile(r'^function\s+(\w)\s+takes\s+nothing|[\w,*\s*]+\s+returns\s+nothing|\w\s*$')


def apply(path, pattern, excmd):
    assert path != None
    assert pattern != None

    format_pattern = '-N' == excmd
    format_number = '-n' == excmd or not format_pattern

    entry_format = None
    if format_pattern:
        entry_format = '{tag_name}\t{file_name}\t{pattern}'
    elif format_number:
        entry_format = '{tag_name}\t{file_name}\t{number}'
    else:
        entry_format = '{tag_name}\t{file_name}\t{number}'

    assert entry_format != None

    f = io.open(path)

    nr = 0
    for n in f:
        nr = nr + 1
        match = pattern.match(n)
        if match:
            g = match.groupdict()
            tag = g['tag']

            print(entry_format.format(tag_name = tag,
                                      file_name = path,
                                      number = nr,
                                      pattern = '/^' + n.rstrip() + '$/'))

    f.close()

def verify(path):
    if (None == path):
        return False

    r = False

    try:
        with io.open(path) as f:
            r = f.readable()
            f.close()
    except IOError:
        r = False

    return r

def main():
    excmd = None
    for o in sys.argv:
        if '-n' == o or '-N' == o:
            excmd = o
            break

    print('!_TAG_FILE_FORMAT\t1')
    print('!_TAG_FILE_SORTED\t0')
    print('!_TAG_PROGRAM_AUTHOR\tVladyslav Bondarenko')
    print('!_TAG_PROGRAM_NAME\tjasstags.py')
    print('!_TAG_PROGRAM_URL\thttps://gitlab.com/vladyslavbond/jasstags')
    print('!_TAG_PROGRAM_VERSION\t0.0-0')

    p = pattern_get()
    i = 1
    j = min(max(0, len(sys.argv)), 8192)
    while(i < j):
        path = sys.argv[i]

        if verify(path):
            apply(path, p, excmd)

        i = i + 1

if __name__ == '__main__':
    main()

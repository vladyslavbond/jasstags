SHELL ?= /bin/sh

srcdir ?= ./

PYTHON3 ?= /bin/python3

CTAGS ?= /bin/ctags
CTAGSFLAGS ?= --options=${srcdir}etc/ctags/jass.ctags

all: test

.PHONY = test
test: tags TAGS

.PHONY = clean
clean:
	rm tags TAGS

tags: ${srcdir}src/jasstags.py ${srcdir}test/war3map.j
	${PYTHON3} $^ > $@

TAGS: ${srcdir}test/war3map.j
	${CTAGS} -f $@ ${CTAGSFLAGS} $^

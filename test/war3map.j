globals
	// nasty leading comment 321
	constant integer USER_N = 0 // nasty trailing comment 123
	constant integer HD_G = 9
	constant integer HD_F = 1
	real hd_stub
	real hd_stub2 = 0.0
	string array hd_stubs
endglobals

function baseline takes nothing returns nothing
endfunction

function honeydew_g takes nothing returns integer
	local integer i = HD_G
	return i
endfunction

function honeydew_f takes nothing returns integer
	local integer i = honeydew_g()
	local integer j = HD_F
	return j + j
endfunction

function main takes nothing returns nothing
	local integer i = 0 // nasty trailing comment 123
	local integer j
	set hd_stub = 0.025
	set j = honeydew_f()
	set hd_stubs[i] = "Hello, world!"
	return
endfunction
